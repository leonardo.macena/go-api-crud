package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// user struct
type User struct {
	Name string
}

// Book Struct
type Book struct {
	Id     int
	Title  string
	Author *Author
}

// Author Struct
type Author struct {
	Firstname string
	Lastname  string
}

var books []Book
var users []User

func getBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(books)
	w.WriteHeader(http.StatusAccepted)
}

func getBook(w http.ResponseWriter, r *http.Request) {

}

func createBook(w http.ResponseWriter, r *http.Request) {

}

func updateBook(w http.ResponseWriter, r *http.Request) {

}

func deleteBook(w http.ResponseWriter, r *http.Request) {

}

func main() {
	// init Router
	r := mux.NewRouter()

	//Mock data
	books = append(books, Book{
		Id:    1,
		Title: "senhor dos aneis",
		Author: &Author{
			Firstname: "j",
			Lastname:  "tolkin",
		},
	})

	books = append(books, Book{
		Id:    2,
		Title: "harry potter",
		Author: &Author{
			Firstname: "jk",
			Lastname:  "rolling",
		},
	})

	users = append(users, User{Name: "Merda"})

	//Route Handlers
	r.HandleFunc("/api/books", getBooks).Methods("GET")
	r.HandleFunc("/api/books/{id}", getBook).Methods("GET")
	r.HandleFunc("/api/books/{id}", updateBook).Methods("PUT")
	r.HandleFunc("/api/books/", createBook).Methods("POST")
	r.HandleFunc("/api/books/{id}", deleteBook).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":3000", r))
}
